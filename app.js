module.exports.rootPath = __dirname;
// to create folder same level with 'app.js'
//(!)  put it on top, before call app.use (some import relate to this export) --- cause bug if import not found it

// ------------ (FIX HOMEWORK: run with app. instead Router)
const {
	getFiles,
	validateInput,
	stringifyContent,
	my_writeFile,
	logPasswordFile,
	lookupFileName,
	checkPassword,
	parseContent,
	responseFileInfo,
	deleteFile,
} = require('./routes/api/files_2');
// ------------ (FIX HOMEWORK: run with app. instead Router)
const express = require('express');
const logger = require('./middleware/logger');
const app = express();
const cors = require('cors');

// app.get('/', (req, res) => {
// 	res.send('Hello page');
// });
app.use(cors());

// logging request info
app.use(logger);
// body-parse from POST
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ------------ (ORIGINAL: run with Router '/api/files')

// 1. createFile --- create file with body param and response

// 2. getFiles --- get all files
// 3. getFile --- get a single file by URL param '{filename}'
// 4. deleteFile --- delete single file
// app.use('/api/files', require('./routes/api/files'));

// ------------ (ORIGINAL: run with Router '/api/files')

// ------------ (FIX HOMEWORK: run with app. instead Router)
app.get('/api/files/', getFiles);

app
	.get('/api/files/:filename/:password', lookupFileName)
	.get('/api/files/:filename/:password', checkPassword)
	.get('/api/files/:filename/:password', parseContent)
	.get('/api/files/:filename/:password', responseFileInfo);

app
	.get('/api/files/:filename', lookupFileName)
	.get('/api/files/:filename', checkPassword)
	.get('/api/files/:filename', parseContent)
	.get('/api/files/:filename', responseFileInfo);
app
	.post('/api/files/', validateInput)
	.post('/api/files/', stringifyContent)
	.post('/api/files/', my_writeFile)
	.post('/api/files/', logPasswordFile);

app
	.delete(
		['/api/files/:filename/:password', '/api/files/:filename'],
		lookupFileName
	)
	.delete(
		['/api/files/:filename/:password', '/api/files/:filename'],
		checkPassword
	)
	.delete(
		['/api/files/:filename/:password', '/api/files/:filename'],
		deleteFile
	);
// ------------ (FIX HOMEWORK: run with app. instead Router)

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => console.log('Server is listening ...'));
