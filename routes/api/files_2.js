const path = require('path');
const fsPromises = require('fs/promises');
const express = require('express');
const router = express.Router();
const rootPath = require('../../app.js').rootPath; // (!) don't know why it doesn't work normally when exports value --- it likes 'require' happen before 'export'
// ----- module, package for PARSING content
const util = require('util'); // support .js
const YAML = require('yaml'); //support .yaml
const xml2js = require('xml2js'); //support .xml
const builder = new xml2js.Builder();
const parser = new xml2js.Parser();
// ----- module, package for PARSING content

// --------------------------POST '/api/files--------------------------START
// /^[^\\\/:*?"<>|]+\.[^.\\\/:*?"<>|]+$/i
// prevent some special character in window
// require extension === only allow when non-dot character behind the last dot
// KNOW ISSUE: there're more cases (reservered words,...) + other OS
// better way: let OS process itself --- but i dont know how to define that Error --- (I've known)

// SOLUTION: i guess OS will return ENOENT when invalid name
// (1) first check if folder "container_files" exist
// (2) if yes, will response 400 --- "filename is invalid"

// (!) don't know how POST 'form-data' work + read them
// response.body.message
// ------------------------------------------------
const regexFilename = /^[^\\\/:*?"<>|]+\.[^.\\\/:*?"<>|]+$/i;
const regexExtension = /\.(log|txt|json|yaml|xml|js)$/i;
const regexPassword = /^[\w\d-_]+$/i;
// (UPDATED) support '-' '_'
// password: not allow special characeter --- i don't know how to handle when input via "url param"
// password: reverse ">" "|" to separate in file log password

const [containerFileFolderName, logPassFolderName] = [
	'container_files',
	'log_security',
];
const validateInput = (req, res, next) => {
	const { filename, content, password } = req.body;
	// (!) need a way to combine condition --- make it cleaner because response same 'message'
	// check empty
	if (!filename || !content) {
		res.status(400).json({
			message: "Please specify 'filename' and 'content' parameter",
		});
		return;
	}
	// validate filename based on Window rule of "special character"
	if (!regexExtension.test(filename)) {
		res.status(400).json({
			message: 'We only support 6 extensions (log|txt|json|yaml|xml|js)',
		});
		return;
	}
	// only support 6 extensions
	if (!regexFilename.test(filename)) {
		res.status(400).json({
			message: 'Invalid filename in Window --- checked by Me',
		});
		return;
	}

	// only validate if 'password' has been input
	if (password && !regexPassword.test(password)) {
		res.status(400).json({
			message: `Password contains only [a-Z] [0-9] --- ruled by Me`,
		});
		return;
	}
	next();
};

// parse content --- convert JSON response to data fit to 'filename extension'
const stringifyContent = (req, res, next) => {
	// I will attach to req.body._stringifiedContent --- below middlerware will use (content = req.body._stringifiedContent)
	const { filename } = req.body;
	// conver JSON object --> JS-object-like string
	const content =
		typeof req.body.content === 'object'
			? JSON.stringify(req.body.content)
			: req.body.content;
	const [_, extension] = filename.match(regexExtension); // (!) I not confident in my regex, return [".yaml", "yaml"] @@
	switch (extension) {
		// 1/6.js
		case 'js':
			// (1) JSON object -> JS object
			// (2) JS object -> string
			// writeFile only accept 'string'
			// util.inspect() represent JS object as string
			req.body._stringifiedContent = util.inspect(JSON.parse(content));
			break;
		// 2/6.yaml
		case 'yaml':
			// (1) JSON -> YAML
			// (2) YAML -> string
			req.body._stringifiedContent = YAML.stringify(YAML.parse(content));
			break;
		// 3/6 .sml
		case 'xml':
			// 'builder' convert JS object --> XML
			req.body._stringifiedContent = builder.buildObject(JSON.parse(content));

			break;
		default:
			// 6/6 (.txt .json .log) --- handler with JSON.stringify() above
			// I believe .log same .txt --- just plain text
			req.body._stringifiedContent = content;
	}
	next();
	// 4.xml
};
// create or overwrite
const my_writeFile = async (req, res, next) => {
	// (?) need a way to pass argument directly between middlewares
	const { filename, password } = req.body;
	const _stringifiedContent = req.body._stringifiedContent;
	try {
		await fsPromises.writeFile(
			path.join(rootPath, containerFileFolderName, filename),
			_stringifiedContent // need this when 'content' is JSON
			// (!) i need a way to just run when content is JSON --- .stringify() will include ("") in normal string
		);
		// just excute 'logging' middleware if has password
		if (password) return next();
		res.status(200).json({
			message: 'File created successfully --- without password (︶ω︶)',
		});
	} catch (err) {
		if (err?.code === 'ENOENT') {
			// Error: when folder "container_files" not exist
			// Handle: create folder and recurse function again
			try {
				await fsPromises.mkdir(path.join(rootPath, containerFileFolderName));
				console.log(
					`FIXED ERROR: '${containerFileFolderName}' directory has been created.`
				);
				my_writeFile(req, res, next);
			} catch (err_dir) {
				// Error: when folder "container_files" exist but still Error
				// ...  invalid filename checked by OS system just 1-of-cases
				if (err_dir?.code === 'EEXIST') {
					res.status(400).json({
						message: 'Invalid filename in OS --- checked by Operator System',
					});
				} else {
					next(err_dir);
				}
			}
		} else {
			next(err);
		}
	}
};

const logPasswordFile = async (req, res, next) => {
	const { filename, content, password } = req.body;
	try {
		await fsPromises.appendFile(
			// this "file name + password" structure created by myself
			// require string method .indexOf + .slice to read it --- in 'getFile.js'
			path.join(rootPath, logPassFolderName, 'password_files.txt'),
			`${filename}|${password}>`
		);
		res.status(200).json({
			message: 'File created + Set password successfully ┌༼ ˵ ° ᗜ ° ˵ ༽┐',
		});
	} catch (err) {
		if (err?.code === 'ENOENT') {
			// Error: when folder "container_files" not exist
			// Handle: create folder and recurse function again
			try {
				await fsPromises.mkdir(path.join(rootPath, logPassFolderName));
				console.log(
					`FIXED ERROR: '${logPassFolderName}' directory has been created.`
				);
				logPasswordFile(req, res, next);
			} catch (err_dir) {
				next(err_dir);
			}
		}
	}
};

// --------------------------POST '/api/files'--------------------------END

// --------------------------GET '/api/files'--------------------------START

// return all filename in 'container_files' as array
// response.body.message
// response.body.files
// const [file] = response.body.files;
// const filename = 'notes.txt';

const getFiles = async (req, res, next) => {
	try {
		const files = await fsPromises.readdir(
			path.join(rootPath, containerFileFolderName)
		);
		res.status(200).json({
			message:
				files.length > 0
					? 'Success'
					: `I didn't steal your files, they're not exist ╮ (. ❛ ᴗ ❛.) ╭`,
			files,
		});
	} catch (err) {
		if (err?.code === 'ENOENT') {
			// copy from 'my_writeFile' --- when folder no exist yet
			try {
				await fsPromises.mkdir(path.join(rootPath, containerFileFolderName));
				console.log(
					`FIXED ERROR: '${containerFileFolderName}' directory has been created.`
				);
				getFiles(req, res, next);
			} catch (err_dir) {
				next(err_dir);
			}
		} else {
			next(err);
		}
	}
};

// --------------------------GET '/api/files'--------------------------END

// --------------------------GET '/api/files/:filename'--------------------------START
// response.body.message;
// response.body.filename;
// response.body.content;
// response.body.extension;
// response.body.uploadedDate;

const lookupFileName = async (req, res, next) => {
	const { filename } = req.params;
	const filePath = path.join(rootPath, containerFileFolderName, filename);
	try {
		// _rawContent used to convert to JSON in 'parseContent' middleware
		req.body._rawContent = await fsPromises.readFile(filePath, 'utf8');
		next();
	} catch (err) {
		if (err?.code === 'ENOENT') {
			res.status(400).json({
				message: `No file with name '${path.basename(
					filePath
				)}'. Try creating it.`,
			});
		} else {
			next(err);
		}
	}
};

// check password --- scan fileName in log_password.txt
const checkPassword = async (req, res, next) => {
	const { filename } = req.params;
	let indexSymbolStart, indexSymbolEnd;
	try {
		const passLog = await fsPromises.readFile(
			path.join(rootPath, logPassFolderName, 'password_files.txt'),
			'utf8'
		);
		const indexFilname = passLog.lastIndexOf(filename); //lastIndexof to update the 'newest' password
		if (indexFilname === -1) {
			// (1) file don't have password: next()
			console.log(`File '${filename}' doesn't have password. Let's party.`);
			return next();
		} else if (!req.params.password) {
			// (2) file has password
			// (2.1) Validate: 'empty pass' --- message: "file has password, use GET '/api/{filename}/{password}"
			res.status(400).json({
				message:
					"This file require 'password', try [GET '/api/{filename}/{password}]",
			});
		} else {
			// (2.2) Run function check: 'wrong pass' --- message: "File forget you when you forget password"
			// those 2 symbol to separate is reserverd by me with regexPassword + regexFilenma
			indexSymbolStart = passLog.indexOf('|', indexFilname);
			indexSymbolEnd = passLog.indexOf('>', indexFilname);
			const password = passLog.slice(indexSymbolStart + 1, indexSymbolEnd);
			if (req.params.password === password) {
				console.log(`'${filename}' has been unlocked. Roger!`);
				return next();
			} else {
				res.status(400).json({
					message: 'WRONG PASSWORD: file forgets you when you forget password',
				});
			}
		}
	} catch (err) {
		// hanlder '.readFile'
		if (err?.code === 'ENOENT') {
			console.log(
				'Might be no files set password --> no folder and log file created yet'
			);
			next();
		} else {
			next(err);
		}
	}
};

// parse content --- convert 'rawContent' from 'readFile' to JSON response
const parseContent = async (req, res, next) => {
	const { filename } = req.params;
	const rawContent = req.body._rawContent;
	const filePath = path.join(rootPath, containerFileFolderName, filename);
	const extension = path.extname(filePath).slice(1); // get rid of '.' dot

	const switchAsyncFunction = async () => {
		switch (extension) {
			// 1/6.js
			case 'js':
				req.body._parsedContent = rawContent;
				// (?) it workds might be because after I writeFile, .js be treated as 100% string
				break;
			// 2/6.yaml
			case 'yaml':
				req.body._parsedContent = YAML.parse(rawContent);
				// (!) don't know which type will be affteced "swagger: \"2.0\"\ninfo:\n"
				break;
			// 3/6.json
			case 'json':
				req.body._parsedContent = JSON.parse(rawContent);
				// (!) don't know why rawContent === "{\"message\": \"jsondata\"}"
				break;
			// 4/6.yaml
			case 'xml':
				try {
					const data = await parser.parseStringPromise(rawContent);
					req.body._parsedContent = data;
				} catch {
					console.log(`Something wrong when parse .xml --- Don't call me`);
				}
				break;
			default:
				// 6/6 (.txt, .log)
				// (!) newline will return \n don't know how to handler
				req.body._parsedContent = rawContent;
		}
	};
	await switchAsyncFunction();
	// make 'switch' async to use await when parseXML Promise --> prevent break;
	// make this scope async --> prevent next()

	next();
};

const responseFileInfo = async (req, res, next) => {
	// console.log('{"message": "jsondata"}');
	// console.log(req.body._parsedContent);
	// (!) i don't know what happen with 'response' --- can cause error when compare response result
	// to make response .json file in JSON format --- i have to parseContent at JS object
	// then res.status(200).json() will convert JS object to JSON format
	// (?) I don't know how to look into 'response' to see what it return
	const { filename } = req.params;
	const content = req.body._parsedContent;
	const filePath = path.join(rootPath, 'container_files', filename);
	const extension = path.extname(filePath).slice(1); // get rid of '.' dot

	try {
		const stat = await fsPromises.stat(filePath);
		res.status(200).json({
			message: 'Success',
			filename,
			content,
			extension,
			uploadedDate: stat.birthtime,
		});
	} catch (err) {
		next(err);
	}
};

// --------------------------GET '/api/files/:filename'--------------------------END

// --------------------------DELETE '/api/files/:filename'--------------------------START
const deleteFile = async (req, res, next) => {
	const { filename } = req.params;
	const filePath = path.join(rootPath, containerFileFolderName, filename);
	try {
		await fsPromises.unlink(filePath);
		res.status(200).json({
			message: `File '${filename} has been deleted.`,
		});
	} catch (err) {
		next(err);
	}
};
// --------------------------DELETE '/api/files/:filename'--------------------------END
router.get('/', getFiles);

// create file with body param {filename, content, password}
router
	.post('/', validateInput)
	.post('/', stringifyContent)
	.post('/', my_writeFile)
	.post('/', logPasswordFile);

router
	.get('/:filename', lookupFileName)
	.get('/:filename', checkPassword)
	.get('/:filename', parseContent)
	.get('/:filename', responseFileInfo);

router
	.get('/:filename/:password', lookupFileName)
	.get('/:filename/:password', checkPassword)
	.get('/:filename/:password', parseContent)
	.get('/:filename/:password', responseFileInfo);

router
	.delete(['/:filename', '/:filename/:password'], lookupFileName)
	.delete(['/:filename', '/:filename/:password'], checkPassword)
	.delete(['/:filename', '/:filename/:password'], deleteFile);
// (?) use path 'array' will check both --- i think there another way to make paths cleaner when nesteing like app.use() --> route.get(...)
// ---------------------------------------------

module.exports = {
	getFiles,
	validateInput,
	stringifyContent,
	my_writeFile,
	logPasswordFile,
	lookupFileName,
	checkPassword,
	parseContent,
	responseFileInfo,
	deleteFile,
};

// Support: check new password --- using lastIndexOf to check 'newest' pass
// Support: update password --- createFile again will update password

// KNOW ISSUE: don't accept remove 'password' after set --- require a way to delete and prevent check password
// You can hack: by delete log_security folder --- no one know which file has been set password -.-

// DELETE file: (FIXED) can delete without password
// + delete not remove password

// What I fix this time?
// 0. set res.status(200)
// 1. delete app.get('/') --- which displayed 'Hello page' text
// 2. separate ['/:filename', '/:filename/:password']
// 3. use try, catch instead .then for .xml parse
// 4. use CORS package enable for all route
// 5. (hack) included folder 'container_files' --- if it works, i'll report that situation. Non-sense
// 6. call directly not through 'router'
// 7. reorganize, put longer routes above '/filename/password' -> '/filename'
// 8. use process.env.PORT
