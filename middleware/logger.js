const rootPath = require('./../app').rootPath;
const fsPromises = require('fs/promises');
const path = require('path');

// logging request info
// (1) write to launch.log
// (2) console.log
const logger = async (req, res, next) => {
	try {
		await fsPromises.appendFile(
			path.join(rootPath, 'launch.log'),
			`Request_URL: ${req.protocol}://${req.get('host')}${req.originalUrl},
Request_type: ${req.method},
Request_date: ${new Date()},
---
`
		);
		// console.log({
		//     Request_URL: `${req.protocol}://${req.get('host')}${req.originalUrl}`,
		//     Request_type: req.method,
		//     Request_date: new Date(),
		// });
		next();
	} catch (err) {
		console.log(`Can't write launch.log`);
		console.log(err);
		next();
	}
};

module.exports = logger;

// --------------
// KNOW ISSUE: newest request will be placed at bottom --- need something like .unshift()
